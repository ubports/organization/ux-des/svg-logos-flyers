# svg logos flyers 

A temptative to have all designs, bits and pieces together.

## Add the missing designs

Contact or make a Merge Request to add the missing or new designs.

## Banners and Device backgrounds

Those designs are allocated in [this repo](https://gitlab.com/ubports/organization/ux-des/design-banners)

## License
Except for the Ubuntu and Canonical material, all works here are published under GNU GENERAL PUBLIC LICENSE version 3

